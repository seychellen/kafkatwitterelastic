# Kafka-Twitter-Elastic-Search

### Web-Links
* [Twitter Java Client](https://github.com/twitter/hbc)
* [Twitter API Credentials](https://developer.twitter.com/)
* [ElasticSearch Java Client](https://www.elastic.co/guide/en/elasticsearch/client/java-rest/6.4/java-rest-high.html)
* [ElasticSearch setup](https://www.elastic.co/guide/en/elasticsearch/reference/current/setup.html)


### Twiter-Client
* `curl -X GET -H "Authorization: Bearer <BEARER TOKEN>" "https://api.twitter.com/2/tweets/20"`
* `curl -X GET -H "Authorization: Bearer <BEARER TOKEN>" "https://api.twitter.com/2/tweets/440322224407314432"`
* `curl -X GET -H "Authorization: Bearer <BEARER TOKEN>" "https://api.twitter.com/2/tweets/440322224407314432?expansions=author_id,attachments.media_keys"` mit author_id und Attachments


You will need Elevated Access, not only Essential!
https://developer.twitter.com/en/portal/products/elevated