package com.javacook.twitter2kafka;

import com.google.common.collect.Lists;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.*;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import static java.lang.Integer.*;
import static org.apache.kafka.clients.producer.ProducerConfig.*;

public class TwitterProducer {

    private final static String CONSUMER_KEY = "yCgxF5DlYPut5nr2hKGIHIC6";
    private final static String CONSUMER_SECRET = "wmUOfDZWsssXMj81ND4wUrVMtaIfec036R9t1d9wCtoeOwcVV";
    private final static String ACCESS_TOKEN = "1745402-Oq0RZi6P72jrjLo0YFHdrZH2QP75g6OSg2hscpsvZ";
    private final static String ACCESS_TOKEN_SECRET = "qDFOBZj4hOx7KWCJbM7vOPySndHiqTrTFtBuMe3xUzbZ";

    public void  run()  {
        // Set up your blocking queues: Be sure to size these properly based
        // on expected TPS of your stream
        BlockingQueue<String> msgQueue = new LinkedBlockingQueue<>(1000);

        Client twitterClient = createTwitterClient(msgQueue);
        twitterClient.connect();

        KafkaProducer<String, String> producer = createKafkaProducer();

        // on a different thread, or multiple different threads....

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Stopping Application...");
            twitterClient.stop();
            producer.close();
            System.out.println("...done.");
        }));

        while (!twitterClient.isDone()) {
            String msg = null;
            try {
                msg = msgQueue.poll(5, TimeUnit.SECONDS);
                System.out.println("Received Message: " + msg);
            } catch (InterruptedException e) {
                e.printStackTrace();
                twitterClient.stop();
            }
            if (msg != null) {
                producer.send(new ProducerRecord<>("twitter_tweets", null, msg),
                (recordMetadata, e) -> {
                    if (e == null) {
                        System.out.println("Received new metadata" +
                                "\nTopic: " + recordMetadata.topic() +
                                "\nPartition: " + recordMetadata.partition() +
                                "\nOffset: " + recordMetadata.offset() +
                                "\nTimestamp: " + recordMetadata.timestamp());
                    } else {
                        System.err.println("Error while producing: " + e);
                    }
                });
            }
        }
        System.out.println("Game over");
    }

    private KafkaProducer<String, String> createKafkaProducer() {
        var properties = new Properties();
        properties.setProperty(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.setProperty(KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        // for a safe producer:
        properties.setProperty(ENABLE_IDEMPOTENCE_CONFIG, "true");
        properties.setProperty(ACKS_CONFIG, "all");
        properties.setProperty(RETRIES_CONFIG, Integer.toString(MAX_VALUE));
        properties.setProperty(MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "5");
        // for a high throughput producer:
        properties.setProperty(COMPRESSION_TYPE_CONFIG, "snappy");
        properties.setProperty(LINGER_MS_CONFIG,"20");
        properties.setProperty(BATCH_SIZE_CONFIG, Integer.toString(32*1024));
        return new KafkaProducer<>(properties);
    }


    public Client createTwitterClient(BlockingQueue<String> msgQueue) {

        // Declare the host you want to connect to, the endpoint, and authentication (basic auth or oauth)
        var hosebirdHosts    = new HttpHosts(Constants.STREAM_HOST);
        var hosebirdEndpoint = new StatusesFilterEndpoint();
        // Optional: set up some followings and track terms
        List<String> terms = Lists.newArrayList("kafka");
        hosebirdEndpoint.trackTerms(terms);

        // These secrets should be read from a config file
        Authentication hosebirdAuth = new OAuth1(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);

        ClientBuilder builder = new ClientBuilder()
                .name("Hosebird-Client-01")  // optional: mainly for the logs
                .hosts(hosebirdHosts)
                .authentication(hosebirdAuth)
                .endpoint(hosebirdEndpoint)
                .processor(new StringDelimitedProcessor(msgQueue));

        Client hosebirdClient = builder.build();
        return hosebirdClient;
    }


    public static void main(String[] args) {
        new TwitterProducer().run();
    }

}
